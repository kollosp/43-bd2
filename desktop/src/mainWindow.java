import javax.swing.*;
import java.awt.*;

public class mainWindow extends JFrame {

    private JPanel mainPanel, employeesPanel, clientsPanel, propertiesPanel;
    private JTabbedPane tabbedPane;

    public static void main(String[] args) {
        new mainWindow();
    }

    mainWindow(){
        setTitle("Aplikacja");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(400,300);
        setResizable(true);
        setLocationRelativeTo(null);

        setComponents();

        setVisible(true);
    }

    void setComponents(){
        mainPanel=new JPanel();
        employeesPanel=new JPanel();
        clientsPanel=new JPanel();
        propertiesPanel=new propertiesMenu(this.getSize());

        tabbedPane=new JTabbedPane();

        tabbedPane.addTab("Posesje", propertiesPanel);
        tabbedPane.addTab("Klienci",clientsPanel);
        tabbedPane.addTab("Pracownicy",employeesPanel);

        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(tabbedPane);

        setContentPane(mainPanel);
    }
}
