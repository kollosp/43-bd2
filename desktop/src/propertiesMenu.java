import javafx.geometry.VerticalDirection;
import javafx.scene.layout.Border;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.basic.BasicBorders;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

public class propertiesMenu extends JPanel{
    private JPanel menuControls;
    private JPanel objectField;
    private JScrollPane scrollPane;

    //MENU
    private JTextField queryField;
    //filtry
    private JRadioButton sold;
    private JRadioButton rent;
    private JRadioButton forSale;
    private JRadioButton forRent;
    private JButton search;
    private JButton add;
    //POMOCNICZE
    private int rows;
    private int col;
    private int itemNumber;
    private Dimension dim;
    private Border myBorder;
    //TODO dadać wszędzie obramowanie

    private class propertyIcon extends JPanel{
        JTextArea propertyDes;
        JLabel propertyName;
        JPanel photo_desPanel;
        BufferedImage photo;

        propertyIcon(String name, String photoPath){
            propertyDes=new JTextArea(photoPath);
            propertyName=new JLabel(name);
            photo_desPanel=new JPanel();
            photo_desPanel.setLayout(new BorderLayout());

            setLayout(new BorderLayout());
            setPreferredSize(new Dimension(100,150));

            photo_desPanel.add(propertyDes);
            add(photo_desPanel,BorderLayout.CENTER);
            add(propertyName,BorderLayout.SOUTH);
        }
    }

    propertiesMenu(Dimension dimension){
        setBackground(Color.DARK_GRAY);
        itemNumber=1;

        dim=dimension;
        setLayout(new BorderLayout());
        setComponents();
        reDrawIcons();

        addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(ComponentEvent componentEvent) {
                reDrawIcons();
            }

            @Override
            public void componentMoved(ComponentEvent componentEvent) {

            }

            @Override
            public void componentShown(ComponentEvent componentEvent) {

            }

            @Override
            public void componentHidden(ComponentEvent componentEvent) {

            }
        });
    }

    void setComponents(){
        menuControls=new JPanel();
        objectField=new JPanel(null);
        scrollPane=new JScrollPane(objectField);

        queryField=new JTextField();
        queryField.setText("parametry wyszukiwania");
        queryField.setHorizontalAlignment(SwingConstants.CENTER);
        queryField.setPreferredSize(new Dimension(150,20));
        sold=new JRadioButton("Sprzedane");
        forSale=new JRadioButton("Na sprzedarz");
        rent=new JRadioButton("Wynajete");
        forRent=new JRadioButton("Pod wynajem");
        search=new JButton("Szukaj");
        add=new JButton("Dodaj posesje");

        JPanel jj=new JPanel();
        jj.setPreferredSize(new Dimension(180,0));
        menuControls.setLayout(new GridLayout(7,1,2,2));

        menuControls.add(queryField);
        menuControls.add(sold);
        menuControls.add(forSale);
        menuControls.add(rent);
        menuControls.add(forRent);
        menuControls.add(search);
        menuControls.add(add);

        menuControls.setPreferredSize(new Dimension(180,220));
        jj.add(menuControls);

        add(jj,BorderLayout.WEST);
        add(scrollPane,BorderLayout.CENTER);
    }

    void addEmptyPanels(int n){
        for(int i=0;i<n;i++)
            objectField.add(new JPanel());
    }

    void addIcons(){
        //TODO wyslij zapywanie do bazy
        //TODO ustaw liczbe elementow
        objectField.add(new propertyIcon("jeden","1"));
        objectField.add(new propertyIcon("dwa","2"));
        objectField.add(new propertyIcon("trzy","3"));
        objectField.add(new propertyIcon("cztery","4"));
        objectField.add(new propertyIcon("piec","5"));
        objectField.add(new propertyIcon("szesc","6"));
        objectField.add(new propertyIcon("siedem","7"));
        itemNumber=7;
    }

    void reDrawIcons(){
        objectField.updateUI();
        int width=this.getWidth();
        int height=this.getHeight();

        if(width==0)
            width=dim.width;

        width-=180;

        if(height==0)
            height=dim.height;

        col=(width-10)/110;
        if(col==1)
            rows=itemNumber;
        else if(itemNumber%col==0)
            rows=itemNumber/col;
        else
            rows=(itemNumber/col)+1;

        if(rows-1<(height/150))
            rows=height/150;

        objectField.removeAll();
        //GridLayout GL=new GridLayout(rows,col,5,5);

        objectField.setLayout(new GridLayout(rows,col,5,5));
        addIcons();
        addEmptyPanels((rows*col)-itemNumber);
        System.out.println("Width: "+width+" Col: "+col+"\nHeight: "+height+" Rows: "+rows);
    }
}
