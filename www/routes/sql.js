const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 10;

const c = require('../const')

let pool = mysql.createPool(c.db);

pool.on('connection', function (connection) {
    //console.log("connection created")
});

setInterval(() => {
    pool.getConnection(function(err, connection){
        if (err) {
            console.error("Error: ", err)
            return
        }

        connection.query("SELECT 1", function (err, result) {
            if (err) {
                console.error("Error: ", err)
                return
            }
            //console.log('Server responded to ping')
            connection.release()
        })
    })
}, 10000)

pool.on('release', function (connection) {
    console.log('Connection %d released', connection.threadId);
});

/**
 * Function parses where and make unnecessary changes in sqlString
 * @param sqlString e.x. SELECT * FROM LOGS
 * @param whereSql e.x 1=1 OR 1=2
 */
const parseWhere = function(sqlString, whereSql, config) {
    if(whereSql) {
        sqlString += `WHERE ${whereSql} `
    }

    return sqlString
}

/**
 * Function parses [select | delete | ..] from sqls
 * @param sqlString e.x. SELECT
 * @param from table name or names
 * @param config array contains data to fill ?? and ?
 */
const parseFrom = function (sqlString, from, config) {
    
}

const queries = {

    /**
     * Begin transaction
     * @returns {Promise<unknown>}
     */
    queryBeginTransaction: function(){
        return new Promise(function(resolve, reject) {
            pool.getConnection(function (err, connection) {
                if (err) {
                    console.error("Error: ", err)
                    reject(err)
                    return
                }

                connection.beginTransaction(function(err) {
                    if (err) {
                        reject(err);
                        return
                    }

                    resolve(connection)
                })
            })
        })
    },

    /**
     * commit transaction
     * @param connection
     * @returns {Promise<unknown>}
     */
    queryCommit: function(connection) {
        return new Promise(function(resolve, reject) {
            connection.commit(function(err) {
                if (err) {
                    return connection.rollback(function() {
                        connection.release()
                        reject(err)
                    });
                }
                connection.release()
                resolve()
            });
        })
    },

    /**
     * rollback changes
     * @param connection
     * @returns {Promise<unknown>}
     */
    queryRollback: function(connection) {
        return new Promise(function(resolve, reject) {
            connection.rollback(function() {
                resolve()
                connection.release()
            });
        })
    },

    /**
     *Make a query when transaction is processing
     * @param connection
     * @param sql
     * @param params
     * @returns {Promise<unknown>}
     */
    queryTransaction: function(connection, sql, params){
        return new Promise(function(resolve, reject) {
            connection.query(sql, params, (err, result, fields) => {
                //log sql query
                //console.log("sql: ", q.sql)

                //catch errors
                if(err){
                    reject(err)
                    return
                }
                //release connection and resolve results

                resolve(result, fields)
            })

        })
    },

    /**
     * Gets connection and run query with promise base interface
     * @param sql - sql query with '?' as when param occurs
     * @param params - list of params to be escaped
     * @returns {Promise<unknown>}
     */
    query: function(sql, params) {
        return new Promise(function(resolve, reject) {
            pool.getConnection(function (err, connection) {
                if (err) {
                    console.error("Error: ", err)
                    return
                }

                let q = connection.query(sql, params, (err, result, fields) => {
                    //log sql query
                    //console.log("sql: ", q.sql)

                    //catch errors
                    if(err){
                        reject(err)
                        return
                    }
                    //release connection and resolve results
                    connection.release()
                    resolve(result, fields)
                })
            })
        })
    },

    showTables: function(){
        queries.query("SHOW TABLES", [])
            .then(result => console.log(result))
            .catch(e => console.error(e))
    },

    /**
     * Function generates insert query
     * @param to - table name
     * @param data - object where keys are column names and values are column values
     * @returns {Promise<unknown>}
     */
    insert: function(to, data, connection) {
        return new Promise(function(resolve, reject) {
            let config = []
            let sqlString = "INSERT INTO " + to + " ("

            let keys = Object.keys(data)
            for(let i=0;i<keys.length;++i){
                sqlString += keys[i]
                if(i!=keys.length-1)
                    sqlString+=','
            }

            sqlString += ') VALUES ('

            for(let i=0;i<keys.length;++i){
                sqlString += mysql.escape(data[keys[i]])
                if(i!=keys.length-1)
                    sqlString+=','
            }

            sqlString += ')'


            //transaction
            if(connection){
                queries.queryTransaction(connection, sqlString, config)
                    .then((result, fields) => {
                        resolve([result])
                    })
                    .catch(e => reject(e))
            }
            //no transaction
            else{

                queries.query(sqlString, config)
                    .then((result, fields) => {
                        resolve([result])
                    })
                    .catch(e => reject(e))
            }
        })
    },

    update: function(table, where, data) {
        return new Promise(function(resolve, reject) {
            let config = []
            let sqlString = "UPDATE " + table + " set "

            let keys = Object.keys(data)
            for(let i=0;i<keys.length;++i){
                sqlString += keys[i] + "=" + mysql.escape(data[keys[i]])

                if(i!=keys.length-1)
                    sqlString+=','
            }
            sqlString += " "
            sqlString = parseWhere(sqlString, where)

            queries.query(sqlString, config)
                .then((result, fields) => {
                    resolve([result])
                })
                .catch(e => reject(e))
        })
    },

    /**
     * Function allow to select table from database
     * @param from - table name
     * @param fields - fields to be selected
     * @param where - conditions
     * @param limit - rows count
     * @returns {Promise<unknown>}
     */
    select: function(from, {fields, where, limit}) {
        return new Promise(function(resolve, reject) {
            let config = []
            let sqlString = "SELECT "

            console.log("f", {fields, where, limit} )


            if (fields) config.push(fields)
            else fields = "*"

            config.push(from)
            sqlString += `${fields == "*" ? "*" : "??"} FROM ?? `

            sqlString = parseWhere(sqlString, where, config)



            if(limit) {
                sqlString += `LIMIT ?`
                config.push(parseInt(limit))
            }

            queries.query(sqlString, config)
                .then((result, fields) => {
                    resolve(result)
                })
                .catch(e => reject(e))
        })
    },

    selectProperties: function () {
        return new Promise(function(resolve, reject) {
            let sqlString = "SELECT * FROM PROPERTIES as P, LOCATIONS as L, CLASSES as C WHERE" +
                " P.LOCATION_ID = L.LOCATION_ID AND C.CLASS_ID = P.CLASS_ID"

            queries.query(sqlString, [])
                .then((result, fields) => {
                    resolve(result)
                })
                .catch(e => reject(e))
        })
    },

    selectProperty: function ({propertyId}) {
        return new Promise(function(resolve, reject) {
            let sqlString = "SELECT * FROM PROPERTIES as P, LOCATIONS as L, CLASSES as C WHERE" +
                " P.LOCATION_ID = L.LOCATION_ID AND C.CLASS_ID = P.CLASS_ID AND P.PROPERTY_ID = ? LIMIT 1"

            queries.query(sqlString, [propertyId])
                .then((result, fields) => {

                    if(result.length == 0)
                        reject({error: "property unavailable"})
                    else
                        resolve(result[0])
                })
                .catch(e => reject(e))
        })
    },

    delete: function(from, where){
        return new Promise(function(resolve, reject) {
            let sqlString = "DELETE FROM " + from + " "
            sqlString = parseWhere(sqlString, where)

            queries.query(sqlString, [])
                .then((result, fields) => {
                    resolve(result)
                })
                .catch(e => reject(e))
        })
    },

    /**
     * Function create propoperty and do all db requeste which are needed
     * @param locationVoivodeship - location voivodeship
     * @param locationCity - city
     * @param locationStreet - full address street, local
     * @param purpose
     * @param classId - id of the class row
     * @param surface
     * @param pro_description - unnecessary
     * @param pro_description_2 - unnecessary
     * @param name - unnecessary
     * @returns {Promise<unknown>}
     */
    createProperty: function ({locationVoivodeship, locationCity, locationStreet, purpose, classId, surface, pro_description, pro_description_2, name}, connection=undefined) {
        return new Promise(function (resolve, reject) {

            //inset into location
            queries.insert('LOCATIONS', {
                "VOIVODESHIP": locationVoivodeship,
                "CITY": locationCity,
                "STREET_ADDRESS": locationStreet
            }, connection).then(result => {
                //insert into properies
                queries.insert('PROPERTIES', {
                    "PROPERTY_NAME": name || "",
                    "LOCATION_ID": result[0].insertId,
                    "SURFACE": surface,
                    "CLASS_ID": classId,
                    "PURPOSE" : purpose,
                    "PRO_DESCRIPTION": pro_description || "",
                    "PRO_DESCRIPTION_2": pro_description_2 || "",
                    "AVAILABILITY": "AVAILABLE"
                }, connection).then(result2 => {
                    resolve([result, result2])
                }).catch(e => reject(e))
            }).catch(e => reject(e))

        })
    },

    /**
     * Function creates agreement between client and agent(client with is_agent == 1)
     * @param clientId
     * @param agentId
     */
    createAgreement: function({clientId, agentId, propertyId}) {
        return new Promise(function(resolve, reject) {
            queries.select('D_USERS', {fields:["IS_AGENT"],where: "USER_ID=" + agentId}).then(result => {
                if(result[0].IS_AGENT == 1){
                    queries.insert("AGREEMENTS", {
                        "AGENT_ID": agentId,
                        "CUSTOMER_ID": clientId,
                        "PROPERTY_ID": propertyId,
                    }).then(result2 => resolve([result, result2]))
                        .catch(e => reject(e))
                }
                else reject("agentId = " + agentId + " is not an agent")
            }).catch(e => reject(e))
        })
    },

    createUser: function({name, surname, phone, email, password, username, isAgent}) {
        return new Promise(function(resolve, reject) {
            password = bcrypt.hashSync(password, saltRounds)

            queries.insert('D_USERS', {
                "STATUS": "ACTIVE",
                "NAME": name || "",
                "SURNAME": surname || "",
                "PHONE_NUMBER": phone || "",
                "EMAIL": email || "",
                "IS_AGENT": isAgent || 0
            }).then(result => {
                queries.insert('LOGIN_DATA', {
                    "USERNAME": username,
                    "PASSWORD": password,
                    "USER_ID": result[0].insertId
                })
                    .then(result2 => resolve([result, result2]))
                    .catch(e => reject(e))
            }).catch(e => reject(e))
        })
    },

    verifyUser: function({password, username}) {
        return new Promise(function(resolve, reject) {
            queries.query(
                "SELECT * FROM D_USERS as u, LOGIN_DATA as ld WHERE u.USER_ID=ld.USER_ID AND ld.USERNAME=? LIMIT 1",
                [username])
                .then((result, fields) => {
                    if (bcrypt.compareSync(password, result[0].PASSWORD) && result[0].STATUS == "ACTIVE") {

                        let d = result[0]
                        delete d.PASSWORD

                        resolve({verify: true, user:d})
                    } else {
                        resolve({verify: false})
                    }
                }).catch(e => reject(e))
        })
    },

    /**
     * Function creates new user with is_admin=1 if clientId is null or incorrect. Otherwise it
     * change is_admin field in found client row
     * @param clientId - identifier of client who become admin
     */
    createAgent: function({clientId}) {
        return new Promise(function(resolve, reject) {
            queries.update("D_USERS", "USER_ID="+mysql.escape(clientId), {
                "IS_AGENT": 1
            }).then(result => resolve(result)).catch(e => reject(e))
        })
    },

    /**
     *
     * @param contractId
     * @param date
     * @returns {Promise<unknown>}
     */
    createInvoice: function({contractId, date}) {
        return new Promise(function(resolve, reject) {
            try {
                queries.select("CONTRACTS", {where: "CONTRACT_ID="+contractId})
                    .then(contract => {
                        queries.select("AGREEMENTS", {where: "AGREEMENT_ID="+contract[0].AGREEMENT_ID})
                            .then(agreement => {
                                queries.select("PROPERTIES", {where: "PROPERTY_ID="+agreement[0].PROPERTY_ID})
                                    .then(property => {
                                        queries.select("CLASSES", {where: "CLASS_ID="+property[0].CLASS_ID})
                                            .then(cla => {
                                                let d = new Date

                                                //console.log(contract ,"\n", agreement,"\n", property,"\n", cla)
                                                //console.log( parseFloat(cla[0].VALUE_M2), parseFloat(property[0].SURFACE), parseFloat(cla[0].COST_PER_MONTH))
                                                queries.insert("INVOICES", {
                                                    "CONTRACT_ID": contractId,
                                                    "PAY_DATE": date,
                                                    "AMOUNT_TO_PAY": parseFloat(cla[0].VALUE_M2) * parseFloat(property[0].SURFACE) + parseFloat(cla[0].COST_PER_MONTH),
                                                    "CREATE_DATE": d.toISOString().slice(0,10)
                                                }).then(inserted => {
                                                    resolve(inserted)
                                                }).catch(e => reject(e))
                                            }).catch(e => reject(e))
                                    }).catch(e => reject(e))
                            }).catch(e => reject(e))
                    }).catch(e => reject(e))
            }catch (e) {
                console.error(e)
                reject(e)
            }
        })
    },

    /**
     *
     * @param propertyId
     * @returns {Promise<unknown>}
     */
    deletePropertie: function({propertyId}) {
        return new Promise(function(resolve, reject) {

            queries.select("PROPERTIES", {where: "PROPERTY_ID="+mysql.escape(propertyId)}).then(result => {
                queries.select("AGREEMENTS", {where: "PROPERTY_ID=" + mysql.escape(propertyId)})
                    .then(result1 => {
                        if(result1.length > 0){
                            //set as inactive
                            queries.update("PROPERTIES", "PROPERTY_ID="+mysql.escape(propertyId), {
                                AVAILABILITY: "INACTIVE"
                            }).then(result2 => resolve([result, result1, result2]))
                                .catch(e => reject(e))
                        }else{
                            //delete location and property
                            queries.delete("PROPERTIES", "PROPERTY_ID="+mysql.escape(propertyId))
                                .then(result2 => {
                                    queries.delete("LOCATIONS", "LOCATION_ID="+mysql.escape(result[0].LOCATION_ID))
                                        .then(result3 => resolve([result, result1, result2, result3]))
                                        .catch(e => reject(e))
                                }).catch(e => reject(e))
                        }
                    }).catch(e => reject(e))
            })
        })
    },

    /**
     * Funcion changes AVAILABILITY to INACTIVE. user means admin or client
     * @param userId
     * @returns {Promise<unknown>}
     */
    deleteUser: function({userId}){
        return new Promise(function(resolve, reject) {
            try {
                let resultes = []
                queries.select("D_USERS", {where: "USER_ID=" + mysql.escape(userId)})
                    .then(result => {
                        resultes.push(result)
                        let isAgent = result[0].IS_AGENT
                        let where = result[0].IS_AGENT == 1 ? "AGENT_ID" : "CUSTOMER_ID"

                        queries.select("AGREEMENTS", {where: where + "=" + mysql.escape(userId)})
                            .then(async (result) => {
                                resultes.push(result)

                                if (result.length > 0) {
                                    //set inactive in all properties owned by agent
                                    if (isAgent) {
                                        for (let i in result) {
                                            let r = await queries.update("PROPERTIES", "PROPERTY_ID=" + mysql.escape(result[i].PROPERTY_ID), {
                                                AVAILABILITY: "INACTIVE"
                                            })
                                            resultes.push(r)
                                        }
                                    }
                                    console.log(resultes)
                                    for(let i in result) {
                                        let r = await queries.select("TOURS", {where: "AGREEMENT_ID=" + mysql.escape(result[i].AGREEMENT_ID)})
                                        resultes.push(r)


                                        //remove all future tours
                                        for (let j in r) {
                                            if (r[j].TOUR_DATE > Date.now()) {
                                                let r2 = await queries.delete("TOURS", "TOUR_ID=" + mysql.escape(r[j].TOUR_ID))
                                                resultes.push(r2)
                                            }
                                        }
                                    }

                                    result = await queries.deleteLoginData({userId})
                                    resultes.push(result)
                                    resolve(resultes)
                                } else {
                                    //delete client
                                    queries.delete("LOGIN_DATA", "USER_ID="+mysql.escape(userId))
                                        .then(result => {
                                            resultes.push(result)
                                            queries.delete("D_USERS", "USER_ID=" + mysql.escape(userId))
                                                .then(result => {
                                                    resultes.push(result)
                                                    reject(resultes)
                                                }).catch(e => reject(e))
                                        }).catch(e => reject(e))
                                }
                            }).catch(e => reject(e))
                    }).catch(e => reject(e))
            }catch(e){
                console.error(e)
                reject(e)
            }
        })
    },

    deleteLocation: function({locationId}) {
        return new Promise(function(resolve, reject) {
            try{
                queries.update("LOCATIONS", "LOCATION_ID="+mysql.escape(locationId), {
                    "VOIVODESHIP": "",
                    "CITY": "",
                    "STREET_ADDRESS": ""
                }).then(result => resolve(result)).catch(e => reject(e))
            }catch(e){
                console.error(e)
            }
        })
    },

    deleteClass: function({classId}) {
        return new Promise(function(resolve, reject) {
            try{
                queries.update("CLASSES", "CLASS_ID="+mysql.escape(classId), {
                    "VALUE_M2": 0,
                    "COST_PER_MONTH": 0,
                    "CLASS_DESCRIPTION": ""
                }).then(result => resolve(result)).catch(e => reject(e))
            }catch(e){
                console.error(e)
            }
        })
    },

    deleteLoginData: function({userId}) {
        return new Promise(function(resolve, reject) {
            try{
                queries.select("LOGIN_DATA", {
                    where: "USER_ID="+mysql.escape(userId)
                }).then(result => {
                    queries.update("LOGIN_DATA", "USER_ID="+result[0].USER_ID, {
                        "USERNAME": "",
                        "PASSWORD": ""
                    }).then(result2 => {
                        queries.update("D_USERS", "USER_ID="+mysql.escape(result[0].USER_ID), {
                            "STATUS": "INACTIVE"
                        }).then(result3 => resolve([result, result2, result3]))
                            .catch(e => reject(e))
                    }).catch(e => reject(e))
                }).catch(e => reject(e))
            }catch(e){
                console.error(e)
            }
        })
    },

    /**
     * Function finds or create vidmo user used to make agreements
     */
    findOrCreateVidmoUser: function(){
        return new Promise(function(resolve, reject) {
            try{
                queries.query(
                    "SELECT USER_ID FROM D_USERS as u WHERE NAME = 'Vidmo' AND SURNAME = 'Vidmo' LIMIT 1",
                    [])
                    .then((result, fields) => {
                        if(result.length == 0){
                            //create new vidmo user
                            queries.createUser({name: 'Vidmo', surname: 'Vidmo', phone: "", email: "", username: 'Vidmo', password: "123", isAgent: true})
                                .then(result => {
                                    resolve({vidmoId: result[0][0].insertId})
                                }).catch(e => reject(e))
                        }else{
                            resolve({vidmoId: result[0].USER_ID})
                        }
                    }).catch(e => reject(e))
            }catch(e){
                reject(e)
            }
        })
    },

    createTour: function ({agentId, clientId, propertyId, date}) {
        return new Promise(function (resolve, reject) {
            queries.createAgreement({agentId, clientId, propertyId})
                .then(result => {
                    console.log(result)

                    queries.insert('TOURS', {
                        "AGREEMENT_ID": result[1][0].insertId,
                        "TOUR_DATE": date,
                    }).then(r => resolve([result, r])).catch(e => reject(e))
                })
                .catch(e => reject(e))
        })
    },

    findMeetings: function ({userId}) {
        return new Promise(function (resolve, reject) {
            queries.findOrCreateVidmoUser()
                .then(result => {
                    let vidmoId = result.vidmoId

                    queries.query("SELECT * FROM TOURS as t, AGREEMENTS as a, PROPERTIES as p WHERE " +
                        "t.AGREEMENT_ID = a.AGREEMENT_ID AND a.PROPERTY_ID = p.PROPERTY_ID AND a.CUSTOMER_ID = ?" ,[userId])
                        .then(r1 => {

                            let promises = []

                            for(let i in r1){
                                if(r1[i].AGENT_ID == vidmoId){
                                    //empty promise
                                    promises.push(new Promise(function (resolve, reject) { resolve(false)}))
                                }else {
                                    promises.push(queries.query("SELECT * FROM D_USERS WHERE D_USERS.USER_ID = ?", [r1[i].AGENT_ID]))
                                }
                            }

                            Promise.all(promises).then(results => {
                                for(let i in results){
                                    if(results[i])
                                        r1[i].AGENT = results[i][0]
                                }

                                resolve(r1)
                            }).catch(e => reject(e))
                        }).catch(e => reject(e))
                })
                .catch(e => reject(e))
        })
    },


    findInvoices: function ({userId}) {
        return new Promise(function (resolve, reject) {

            //console.log("findinvoices")

            queries.query("SELECT * FROM CONTRACTS as c, AGREEMENTS as a, PROPERTIES as p WHERE " +
                "c.AGREEMENT_ID = a.AGREEMENT_ID AND a.PROPERTY_ID = p.PROPERTY_ID AND a.CUSTOMER_ID = ?" ,[userId])
                .then(r1 => {

                    //console.log(r1)

                    let promises = []
                    let agents = []

                    for(let i in r1){
                        promises.push(queries.query("SELECT * FROM INVOICES as i WHERE i.CONTRACT_ID = ?", [r1[i].CONTRACT_ID]))
                        promises.push(queries.query("SELECT * FROM D_USERS WHERE D_USERS.USER_ID = ?", [r1[i].AGENT_ID]))
                    }

                    if(promises.length > 0) {
                        Promise.all(promises).then(results => {
                            for (let i=0;i<r1.length;++i) {
                                r1[i].INVOICES = results[2*i]
                                r1[i].AGENT = results[2*i+1][0]
                            }

                            resolve(r1)
                        }).catch(e => reject("1" + e.toString()))
                    }else{
                        resolve(r1)
                    }
                }).catch(e => reject("2" + e.toString()))

        })
    }
}

module.exports = queries
