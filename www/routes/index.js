var express = require('express');
var router = express.Router();

const sql = require('./sql')

module.exports = function(sessionManager) {

    let vidmoId = -1

    sql.findOrCreateVidmoUser().then(r => vidmoId = r.vidmoId).catch(e => console.error(e))

    /**
     *
     * @param query - received query
     * @param expectedQuery - an array contains all required fields names
     * @return query if all required files exists
     */
    const checkQuery = function (query, expectedQuery) {
        let config = query
        for (let i of expectedQuery) {
            if (query[i]) {
                config[i] = query[i]
            } else {
                let exception = "required fields were not specified. url should be: "

                for (let i of expectedQuery) {
                    exception += '/' + i
                }
                exception += " given: " + JSON.stringify(query)

                throw(exception)
            }
        }

        return config
    }


    router.get('/', function (req, res) {

        res.send('index');
    });

    /**
     * get /verifytoken
     * Token in url. If function returns object like: {status: "token valid", body: req.body, session: req.session}, then
     * token was passed correctly
     */
    router.get('/verifytoken', sessionManager.authenticate, function (req, res) {

        res.send({status: "token valid", query: req.query, session: req.session})
    })

    /**
     * post /verifytoken
     * Token in body. If function returns object like: {status: "token valid", body: req.body, session: req.session}, then
     * token was passed correctly
     */
    router.post('/verifytoken', sessionManager.authenticate,  function (req, res) {

        res.send({status: "token valid", body: req.body, session: req.session});
    })

    /**
     * terminology:
     * url scheme: http://simpledomain.com/1/2/3?a=2&b=8
     * http is protocol. Protocol defines some methods e.x. get, post, delete and so on.
     * We will mainly use get and post.
     * simpledomain.com is domain name
     * /1/2/3 is a path
     * a=2&b=8 are query params
     *
     * to run this api endpoint you should request server with the following url:
     * http://<some domain>.pl/select/<table name>?limit=<limit value>&where=<where formula>
     *     &fields=<fields string comma separated>
     *
     */
    router.get('/select/:tableName', sessionManager.authenticate, function (req, res) {
        try {
            let config = req.query

            if (config.fields) {
                config.fields = config.fields.split(',')
                //remove spaces from begginig and end of the fields
                config.fields.forEach((str) => str = str.trim())
            }

            sql.select(req.params.tableName, config)
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })


    /**
     * Function update rows in the table (tableName) where (where clausal) is true
     * url: /update/<tableName>
     * body: {where: "...", col1: val1, col2:val2, ...}
     */
    router.post('/update/:tableName', sessionManager.authenticate, function (req, res) {
        try {
            let config = checkQuery(req.body, ["where"])
            let where = config.where
            delete config['where']
            sql.update(req.params.tableName, where, config)
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * Function insert rows into the table (tableName)
     * url: /insert/<tableName>
     * body: {col1: val1, col2:val2, ...}
     */
    router.post('/insert/:tableName', sessionManager.authenticate, function (req, res) {
        try {
            let config = checkQuery(req.params, ["tableName"])
            sql.insert(config.tableName, req.body)
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * Function deletes rows from the table (tableName) where (where clausal) is true
     * url: /delete/<tableName>
     * body: {where: "..."}
     */
    router.post('/delete/:tableName', sessionManager.authenticate, function (req, res) {
        try {
            let config = checkQuery(req.body, ["where"])
            sql.update(req.params.tableName, config.where)
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * Function creates new property and fill its with default data or th given in query params.
     * @param locationId =
     * @param classId =
     * @param surface =
     * @param purpose =
     * @query = {}
     *
     * Function createProperty row in the database
     * url: /createproperty/
     * body: {locationVoivodeship: val1 ,locationCity: val2, locationStreet: val3, purpose: val4, classId: val5, surface:val6}
     */
    router.post('/createproperty', sessionManager.authenticate, function (req, res) {
        try {
            let config = checkQuery(req.body, ["locationVoivodeship", "locationCity", "locationStreet", "purpose", "classId", "surface"])
            config.pro_description = config.pro_description || ""
            config.pro_description_2 = config.pro_description_2 || ""
            config.name = config.pro_description_2 || ""

            sql.queryBeginTransaction().then(connection => {

                sql.createProperty(config, connection)
                    .then(result => {
                        res.send(result)
                        sql.queryCommit(connection).catch(e => console.error(e))
                    })
                    .catch(e => {
                        res.send({error: e})
                        sql.queryRollback(connection).catch(e => console.error(e))
                    })

            })


        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * Function creates new tour
     * @param agreementId
     * @param date - the agreement date given in format 'dd-mm-yy hh-mm-ss'
     *
     *
     * unnecessary!!! - use insert instead
     * Function creates tour for given agreementId
     * url: /createtour
     * body:{propertyId: val1, date: val2}
     */
    router.post('/createtour',sessionManager.authenticate, function (req, res) {

        console.log("user:", req.user)
        try{
            let config = checkQuery(req.body, ["propertyId","date"])
            sql.createTour({agentId: vidmoId, clientId: req.user.USER_ID, propertyId: config.propertyId, date: config.date})
                .then(result => {res.send(result)})
                .catch(e => res.send({error: e}))

        }catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * unnecessary!!! - use insert instead
     * Function creates new contact for given agreement
     * @param agreementId
     * @data {description} - be careful description is required
     */
    /*router.post('/createcontract/:agreementId', function (req, res) {
        try{

        }catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })*/

    /**
     * Function create agreement between agent and client
     * url: /createagreement
     * body: {agentId: val1 (has to be agent otherwise nothing will be inserted), clientId: val2, propertyId: val3}
     */
    router.post('/createagreement', sessionManager.authenticate, function (req, res) {


        try {
            let config = checkQuery(req.body, ["agentId", "clientId", "propertyId"])
            sql.createAgreement(config)
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * Function creates user and login data
     * url: /createuser
     * body: {username: val1, password: val2, name: val3, surname: val4, phone: val5, email: val6}
     * username and password are required
     */
    router.post('/createuser', sessionManager.authenticate, function (req, res) {
        try {
            let config = checkQuery(req.body, ["username", "password"])
            sql.createUser(config)
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * Function change user into agent
     * url: /createagent
     * body: {clientId: val1}
     */
    router.post('/createagent', sessionManager.authenticate, function (req, res) {
        try {
            let config = checkQuery(req.body, ["clientId"])
            sql.createAgent(config)
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * Function checks if user passed correct username and password
     * url: /verifyuser
     * body: {username: val1, password: val2}
     */
    router.post('/verifyuser', sessionManager.authenticate, function (req, res) {
        /*try {
            let config = checkQuery(req.body, ["username", "password"])
            sql.verifyUser(config)
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }*/

        res.send({token: req.session.token})
    })

    /**
     * Function set user client into agent
     * url: /createagent
     * body: {clientId: val1}
     */
    router.post('/createagent/:userId', sessionManager.authenticate, function (req, res) {
        try {
            sql.update("D_USERS", "USER_ID=" + req.params.userId, {"IS_AGENT": 1})
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * Function creates invoice and calculates cost
     * url: /createinvoice
     * body: {contractId: val1, date: val2}
     */
    router.post('/createinvoice', sessionManager.authenticate, function (req, res) {
        try {
            let config = checkQuery(req.body, ["contractId", "date"])
            sql.createInvoice(config)
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * Function deletes (set username and password to empty) user login data for username
     * url: /deletelogindata
     * body: {userId: val1}
     */
    router.post('/deletelogindata', sessionManager.authenticate, function (req, res) {
        try {
            let config = checkQuery(req.body, ["userId"])
            sql.deleteLoginData(config)
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * Function deletes location (set all data to empty)
     * url: /deletelocation
     * body: {locationId: val1}
     */
    router.post('/deletelocation', sessionManager.authenticate, function (req, res) {
        try {
            let config = checkQuery(req.body, ["locationId"])
            sql.deleteLocation(config)
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * Function deletes class (set all data to empty)
     * url: /deleteclass
     * body: {classId: val1}
     */
    router.post('/deleteclass', sessionManager.authenticate, function (req, res) {
        try {
            let config = checkQuery(req.body, ["classId"])
            sql.deleteClass(config)
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * Function deletes user
     * url: /deleteclass
     * body: {classId: val1}
     */
    router.post('/deleteuser', function (req, res) {
        try {
            let config = checkQuery(req.body, ["userId"])
            sql.deleteUser(config)
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * Function finds properties with all connected data from others tables
     * url: /selectProperties
     * body: {}
     */
    router.post('/selectProperties', function (req, res) {
        try {
            sql.selectProperties()
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * Function finds one property with all connected data from others tables and photos
     * url: /selectProperty
     * body: {}
     */
    router.post('/selectProperty', function (req, res) {
        try {

            let config = checkQuery(req.body, ["propertyId"])
            sql.selectProperty(config)
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * Function deletes property
     * url: /deleteclass
     * body: {classId: val1}
     */
    router.post('/deleteproperty', function (req, res) {
        try {
            let config = checkQuery(req.body, ["propertyId"])
            sql.deletePropertie(config)
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * Function finds meeting for logged user
     * url: /findmeetings
     * body: {}
     */
    router.post('/findmeetings', sessionManager.authenticate, function (req, res) {
        try {
            sql.findMeetings({userId: req.user.USER_ID})
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })

    /**
     * Function finds invoices for logged user
     * url: /findinvoices
     * body: {}
     */
    router.post('/findinvoices', sessionManager.authenticate, function (req, res) {
        try {
            sql.findInvoices({userId: req.user.USER_ID})
                .then(result => res.send(result))
                .catch(e => res.send({error: e}))
        } catch (e) {
            console.error(e)
            res.send({error: e})
        }
    })


    router.post('/logout', sessionManager.logout, function (req, res) {
        res.send({});
    })

    router.get('/time', function (req, res) {

        sql.select("LOGS", [])
        res.send({time: Date.now()});
    });


    router.use((req, res, next) => {
        next({
            status: 404,
            message: 'Not Found',
        });
    });

    router.use((err, req, res, next) => {
        if (err.status === 404) {
            return res.status(400).send('404');
        }

        if (err.status === 500) {
            return res.status(500).send('500');
        }

        next();
    });

    return router;
}
