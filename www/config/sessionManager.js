
module.exports = (verifyFunction) => {

    let users = {/*
        id: <int>,
        timestamp: date
    */}

    //remove users who are inactive
    setInterval(() => {
        let d = new Date()

        for(let i=0;i<10;++i)
            d.setMinutes(d.getMinutes()-1)

        for(let i in users) {
            if(users[i].timestamp < d){
                delete users[i]
            }
        }
    }, 10000)

    const genToken = () => {
        return Date.now()
    }

    /**
     * Middleware function used to check if the user is logged in
     * @param req
     * @param res
     * @param next
     */
    const authenticate = (req, res, next) => {

        if(req.body != undefined && req.body.password != undefined && req.body.username != undefined){

            //create req.session.token
            verifyFunction(req.body.username, req.body.password, (user, error) => {
                if(error){
                    res.status(200).json({success: false, error: 'Bad credentials, ' + error.message});
                    return
                }

                let token = genToken()
                users[token] = user
                users[token].timestamp = new Date()

                req.session.token = token
                req.user = user
                next()
                return
            })

            return
        }

        if(!req.body) req.body = {}
        if(!req.query) req.query = {}
        let token = req.session.token

        if(!token)
            token = req.body.token

        if(!token)
            token = req.query.token

        //verify token - web user
        if(token){

            let d = users[token]
            if(d) {
                d.timestamp = new Date()
                req.user = d
                next()
            }
            else {
                res.status(200).json({success: false, error: 'User logged out'});
            }

            return
        }

        res.status(401).json({success: false, error: 'Unauthenticated'});
    }

    const initialize = (req, res, next) => {
        if(req.session){
            if(req.session.token){
                let d = users[req.session.token]
                if(d){
                    d.timestamp = new Date()
                }
            }
        }

        next()
    }

    const logout = (req, res, next) => {

        if(!req.body) req.body = {}
        let token = req.session.token || req.body.token

        if(users[token])
            delete users[token]

        next()
    }

    return {initialize, authenticate, logout}
}