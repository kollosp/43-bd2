import './stylesheets/style.sass'

import Vue from 'vue'
import VueRouter from 'vue-router'

import index from './components/index.vue'
import mainView from './components/mainView.vue'
import apiView from './components/apiView.vue'
import browseView from './components/browseView.vue'
import loginView from './components/loginView.vue'
import registerView from './components/registerView.vue'
import panelView from './components/panelView.vue'
import panelInvoicesView from './components/panelInvoicesView.vue'
import panelMeetingsView from './components/panelMeetingsView.vue'
import welcomeView from './components/welcomeView.vue'
import propertyView from "./components/propertyView.vue"
import thanksMeetingView from "./components/thanksMeetingView.vue"

import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'

Vue.use(VueRouter)

let router = new VueRouter({
    routes: [
        { path: '/', component: mainView },
        { path: '/browse', component: browseView },
        { path: '/api', component: apiView },
        { path: '/login', component: loginView },
        { path: '/register', component: registerView},
        { path: '/panel', component: panelView, children: [
            { path: 'meetings', component: panelMeetingsView},
            { path: 'invoices', component: panelInvoicesView},
        ]},
        { path: '/welcome', component: welcomeView},
        { path: '/thanksmeeting', component: thanksMeetingView},
        { path: '/property/:id', component: propertyView},
    ]
})


let app = new Vue({
  el: "#app",
  mixins: [index],
  router: router,
})
