const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const { VueLoaderPlugin } = require('vue-loader')


module.exports = {
	entry: './frontend/index.js',
	output: {
		path: path.resolve(__dirname, 'public'),
		filename: 'main.bundle.js'
	},
	module: {
		rules: [
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env']
					}
				}
			},{
				test: /\.s[ac]ss$/i,
				use: [
					MiniCssExtractPlugin.loader,
					{loader: 'css-loader', options: { sourceMap: true } },
					'sass-loader',
				],
			},{
				test: /\.vue$/,
				loader: 'vue-loader'
			},
		]
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: "./frontend/views/index.html"
		}),
		//new ExtractTextPlugin('css/styles.css'),
		new MiniCssExtractPlugin({
			filename: 'styles.css',
			chunkFilename: '[id].css',
		}),
		new VueLoaderPlugin(),
	],
	watch: false
};
