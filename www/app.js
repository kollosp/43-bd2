const createError = require('http-errors')
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const session = require('express-session')
const sql = require('./routes/sql')

const indexRouter = require('./routes/index')
const usersRouter = require('./routes/users')

const sessionManager = require('./config/sessionManager')(function(username, password, done) {

    console.log("verifying", username, password)

    sql.verifyUser({username, password})
        .then(result => {

            if(result.verify == true) {
                return done(result.user);
            }
            else {
                return done({}, {message: 'Incorrect password.'});
            }
        })
        .catch(e => done({},{ message: 'Incorrect username.' }))
})

const app = express()

// view engine setup
app.set('views', path.join(__dirname, 'frontend/views'))
app.set('view engine', 'ejs')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

app.use(session({secret: "DB2Project"}))
app.use(express.static(path.join(__dirname, 'public')))

app.use(sessionManager.initialize)

app.use('/', indexRouter(sessionManager))
app.use('/users', usersRouter)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404))
})



// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.send("Error: 404 <br>" + err.stack)
})

module.exports = app
